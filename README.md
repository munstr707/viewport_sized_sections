Simple snipped to demonstrate a way of rendering multiple sections of content vertically in a way which always has each section filling the viewport individually.

[Demo!](https://munstr707.gitlab.io/viewport_sized_sections/)
